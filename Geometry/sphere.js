const ThreeDimention = require("./threeDimention");

class Sphere extends ThreeDimention {
  constructor(radius) {
    super("Sphere");
    this.radius = radius;
  }

  calculateVolume() {
    super.calculateVolume();
    return (4 / 3) * 3.14 * this.radius ** 3;
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    return 4 * 3.14 * this.radius ** 2;
  }
}

module.exports = Sphere;
