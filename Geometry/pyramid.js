const ThreeDimention = require("./threeDimention");

class Pyramid extends ThreeDimention {
  constructor(base, height) {
    super("Pyramid");
    this.base = base;
    this.height = height;
  }

  //Overiding method
  calculateVolume() {
    super.calculateVolume();
    return (this.base * 2 * this.height) / 3;
  }

  heightPyramid () {
    return Math.sqrt((this.base /2)**2 + this.height ** 2);
  };

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    return (this.base ** 2 + (this.heightPyramid() * this.base * 2));
  }
}

// Ini cuma buat ngetes height pyramid mas, udh bener apa belom wkwkwkkwk
// let test = new Pyramid (6, 4)
// console.log(test.heightPyramid());
module.exports = Pyramid;
