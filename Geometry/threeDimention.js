const Geometry = require("./geometry");

class ThreeDimention extends Geometry {
  constructor(name) {
    super(name, "Three Dimention");
    
    if (this.constructor === ThreeDimention) {
      throw new Error("Cannot instantiate from Abstract Class");
    }
  }

  calculateVolume() {
    console.log(`Calculate ${this.name} Volume!`);
  }


  calculateSurfaceArea() {
    console.log(`Calculate ${this.name} SurfaceArea!`);
  }
}

module.exports = ThreeDimention;
