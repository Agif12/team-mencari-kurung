const ThreeDimention = require("./threeDimention");

class Cone extends ThreeDimention {
  constructor(radius, height) {
    super("Cone");
    this.radius = radius;
    this.height = height;
  }

  calculateVolume() {
    super.calculateVolume();
    return Math.PI * this.radius ** 3;
  }

  linePainter () {
    return Math.sqrt((this.radius ** 2) + (this.height ** 2));
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    return Math.PI * this.radius * (this.radius + this.linePainter());
  }
}

// Ini juga ngetes waktu pencarian kurung linepainter :3
// let test = new Cone (7, 24);
// console.log(test.linePainter());

module.exports = Cone;
