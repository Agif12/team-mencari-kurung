const ThreeDimension = require("./threeDimention");

class Tube extends ThreeDimension {
  constructor(radius, height) {
    super("Tube");
    this.radius = radius;
    this.height = height;
  }

  calculateVolume() {
      super.calculateVolume();
      return 3.14 * this.radius ** 2 * this.height;
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    const phi = 3.14;
    return phi * this.radius * this.radius * this.height;
  }
}

// let newTube = new Tube(7, 24);
// console.log(newTube.calculateCircumference());

module.exports = Tube;
