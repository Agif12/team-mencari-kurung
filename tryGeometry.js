const { Sphere, Pyramid, Tube, Cone  } = require("./Geometry");

let sphereOne = new Sphere (30);
let pyramidOne = new Pyramid (6, 4);
let tubeOne = new Tube (10, 10);
let coneOne = new Cone (7, 24);

// Calculate Volume
let a = sphereOne.calculateVolume();
let b = pyramidOne.calculateVolume();
let c = tubeOne.calculateVolume();
let d = coneOne.calculateVolume();
let e = a + b + c + d;
console.log(b);

// Calculate circumference
let f = sphereOne.calculateSurfaceArea();
let g = pyramidOne.calculateSurfaceArea();
let h = tubeOne.calculateSurfaceArea();
let i = coneOne.calculateSurfaceArea();
let j = f + g + h + i;
console.log(g);
